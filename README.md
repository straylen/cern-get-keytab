# cern-get-keytab

* cern-get-keytab is a utility supported by the linux team which allows a host to communicate to a web service (managed by CDA) to request a Kerberos keytab.
* Many tools at CERN rely on this utility and it is installed on the majority of hosts.  This package is provided through the 'updates' repository on SLC6 and the 'cern' repository on CC7/C8

* This repository is configured for [rpmci](https://gitlab.cern.ch/linuxsupport/rpmci)
* Please refer to the [cc7](https://linuxops.web.cern.ch/distributions/cc7/) and [slc6](https://linuxops.web.cern.ch/distributions/slc6/) build instructions for more details on building internal cern updates.
* Please see the [Kerberos](https://linuxops.web.cern.ch/cheatsheets/kerberos/) page for more details on Kerberos at CERN.