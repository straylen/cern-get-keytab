%{!?dist: %define dist .el7.cern}
Name: cern-get-keytab
Version: 1.1.5
Release: 8%{?dist}
Summary: Utility to acquire Kerberos keytab(s) at CERN
Group: Applications/System
Source: %{name}-%{version}.tgz
License: GPLv3
Vendor: CERN
URL: http://cern.ch/linux/

BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: noarch

%if 0%{?rhel} >= 7
BuildRequires: perl-podlators
%else
BuildRequires: perl
%endif

Requires: msktutil >= 1.0
Requires: perl-WWW-Curl
Requires: perl-Authen-Krb5
Requires: tdb-tools
Provides: cern-merge-keytab

%if 0%{?rhel} >= 6
Requires: CERN-CA-certs >= 20120322-8.slc6
Requires: ca-certificates >= 2010.63-3.el6_1.5
%else
Requires: CERN-CA-certs >= 20120322-8.slc5
Requires: openssl
%endif

%description
%{name} is a CERN utility which stores in local keytab file(s)
host / services identities acquired from CERN Active Directory
KDC. Please note that this tool is CERN specific and of no
use outside CERN network.

%prep
%setup -q

%build
make -C src all

%install
mkdir -p $RPM_BUILD_ROOT

make -C src install DESTDIR=$RPM_BUILD_ROOT/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_sbindir}/cern-get-keytab
%{_sbindir}/cern-merge-keytab
/usr/libexec/cern-get-keytab-samba-workaround
#{_sbindir}/cern-get-dualboot-keytab
%{_mandir}/man3/cern-get-keytab.3*
%{_mandir}/man3/cern-merge-keytab.3*
#{_mandir}/man3/cern-get-dualboot-keytab.3*
%doc src/README

%changelog
* Wed Oct 23 2019 Alex Iribarren <Alex.Iribarren@cern.ch> - 1.1.5-8
- added missing BuildRequires to be able to build on el8

* Wed Oct 02 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-7
- fix "Possible precedence issue" on perl 5.20+ (el8)

* Thu Aug 22 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-6
- do not try to set selinux fcontext if selinux is disabled (on el6)

* Fri Aug 16 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-5
- do not try to set selinux fcontext if selinux is disabled

* Thu Aug 15 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-4
- do not try to set selinux fcontext on remote filesystems

* Mon Jul 22 2019 Ben Morrice <ben.morrice@cern.ch> - 1.1.5-3
- add fix for if a https_proxy is set

* Wed Jul 03 2019 Thomas Oulevey <thomas.oulevey@cern.ch> - 1.1.5-2
- add missing requires for /usr/bin/tdbtool

* Fri Sep 21 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.5-1
- added (cc7/samba-4.7.X) workaround for setting SAMBA password.

* Tue Jun 12 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.4
- make it work for both aliases in "Dynamic Domains" and "Computers"

* Wed Jun 06 2018 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1.3
- added --testsrv option
* Thu Jun 22 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.1
- implement dns alias keytabs generation.
* Thu Mar  9 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.4
- fix msktutil command line build for aliases.

* Thu Feb 16 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.3
- use --dont-update-dnshostname option 

* Wed Feb 15 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.2
- fix undef in usenocanon()

* Mon Feb 13 2017 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 1.0.1
- add user keytab generation

* Tue Apr 26 2016 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.10-1
- fix enctypes processing

* Tue Dec 01 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.9-1
- fix --alias msktutil options.

* Mon Feb 16 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.8-2
- fix undef variable check.

* Fri Feb 13 2015 Thomas Oulevey <thomas.oulevey@cern.ch> 0.9.8
- workaround to not bail out for service ticket request after dyndns check.

* Wed Jan 28 2015 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.6
- workaround to make it work for XXX.dyndns.cern.ch

* Tue Sep 02 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.5
- added cern-merge-keytab to the package.

* Fri Feb 07 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.4
- adding management of samba secrets.tdb
- change of SOAP method to reset passwd.

* Thu Feb 06 2014 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.9.3
- adding time param. to ResetPass request

* Fri Mar 15 2013 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.8
- rewrite of code , to use single KDC.

* Tue Nov 27 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> 0.5.3-1
- update requirement for msktutil >= 0.4.2
- and CERN-CA-certs for 5/6.

* Mon Nov 19 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.5-1
- using upstream msktutil now.

* Thu Oct 25 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.5-2
- production service SOAP URLs
* Wed Oct 24 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.4-1
- version using msktutil-cern.

* Tue Oct 16 2012 Jaroslaw Polok <Jaroslaw.Polok@cern.ch> - 0.0.2-1
- initial test release.
